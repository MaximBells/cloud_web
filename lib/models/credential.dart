class Credential {
  Credential.fromString(String row) {
    List<String> data = row.split('\$');
    url = data[0];
    login = data[1];
    password = data[2];
    date = DateTime.parse(data[3]);
  }

  Credential(
      {
      required this.url,
      required this.login,
      required this.password,
      required this.date});


  String url = "";
  String login = "";
  String password = "";

  DateTime date = DateTime.now();

  @override
  String toString() {
    return '$url\$$login\$$password\$$date';
  }
}
