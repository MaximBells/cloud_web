import 'package:flutter/material.dart';

class Margin extends StatelessWidget {
  const Margin({Key? key, required this.child, required this.geometry})
      : super(key: key);
  final Widget child;
  final EdgeInsetsGeometry geometry;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: geometry,
      child: child,
    );
  }
}
