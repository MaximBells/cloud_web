import 'dart:math';

import 'package:cloud_web/blocks/auth/screens/auth_loading.dart';
import 'package:cloud_web/blocks/auth/screens/register_screen.dart';

import 'package:cloud_web/blocks/loading/data/loading_data.dart';
import 'package:cloud_web/blocks/loading/screens/loading_screen.dart';
import 'package:cloud_web/main/screens/main_screen.dart';
import 'package:cloud_web/static/app_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jwt_decode/jwt_decode.dart';

import 'blocks/auth/screens/login_provider_screen.dart';
import 'blocks/auth/screens/login_screen.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

void main() async {
  runApp(MaterialApp(
    title: 'Password Manager',
    debugShowCheckedModeBanner: false,
    navigatorKey: navigatorKey,
    initialRoute: AppRoute.loading,
    onGenerateRoute: (RouteSettings settings) {
      if (settings.name != null && settings.name!.contains('access_token')) {
        String email = "";
        List<String> array = settings.name!.split('&');
        for (var element in array) {
          if (element.contains('access_token')) {
            Map<String, dynamic> payload = Jwt.parseJwt(element.split('=')[1]);
            if (!Jwt.isExpired(element.split('=')[1])) {
              email = payload['email'] ?? "";
            }
          }
        }
        return CupertinoPageRoute(
            builder: (context) => AuthLoading(
                  email: email,
                ),
            settings: settings);
      }
      switch (settings.name) {
        case AppRoute.loading:
          // print(settings.arguments);
          return CupertinoPageRoute(
              builder: (context) => const LoadingScreen(), settings: settings);
        case AppRoute.login:
          return CupertinoPageRoute(
              builder: (context) => const LoginProviderScreen(),
              settings: settings);
        case AppRoute.register:
          return CupertinoPageRoute(
              builder: (context) => RegisterScreen(), settings: settings);
        case AppRoute.main:
          return CupertinoPageRoute(
              builder: (context) => const MainScreen(), settings: settings);
      }
      return null;
    },
  ));
}
