import 'package:animate_do/animate_do.dart';
import 'package:cloud_web/blocks/auth/screens/login_screen.dart';
import 'package:cloud_web/blocks/loading/data/loading_data.dart';
import 'package:cloud_web/static/app.dart';
import 'package:cloud_web/static/app_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

import '../../auth/screens/login_provider_screen.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ReactionBuilder(
        child: Scaffold(
          body: Center(
            child: Bounce(
                duration: const Duration(milliseconds: 1000),
                delay: const Duration(seconds: 1),
                infinite: true,
                child: FlutterLogo(
                  size: MediaQuery.of(context).size.width * 0.35,
                )),
          ),
        ),
        builder: (context) {
          return reaction((_) {
            LoadingData.loading();
            Future.delayed(const Duration(seconds: 3)).then((value) {
              //change here
              Navigator.pushReplacementNamed(context, AppRoute.login);
            });
          }, (result) {});
        });
  }
}
