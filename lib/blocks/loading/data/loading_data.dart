import 'package:cloud_web/main.dart';
import 'package:cloud_web/static/app.dart';
import 'package:cloud_web/static/app_route.dart';

class LoadingData {
  static Future<void> loading() async {
    await App.initApp();
    if (App.supabase.client.auth.currentSession != null){
      print(App.supabase.client.auth.currentSession!.accessToken);
      print(App.supabase.client.auth.currentSession!.user);
    }
  }
}
