import 'package:flutter/material.dart';

class AuthButton extends StatelessWidget {
  const AuthButton(
      {Key? key,
      required this.text,
      required this.onPressed,
      required this.isPressed})
      : super(key: key);

  final String text;
  final VoidCallback onPressed;
  final bool isPressed;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      height: MediaQuery.of(context).size.height * 0.05,
      constraints: const BoxConstraints(maxWidth: 200),
      width: isPressed
          ? MediaQuery.of(context).size.width * 0.2
          : MediaQuery.of(context).size.width * 0.3,
      duration: const Duration(milliseconds: 250),
      margin: const EdgeInsets.all(24),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
            padding:
                const EdgeInsets.only(left: 48, right: 48, top: 12, bottom: 12),
          ),
          onPressed: () => onPressed(),
          child: isPressed
              ? const Center(
                  child: CircularProgressIndicator(
                  color: Colors.white,
                ))
              : Text(
                  text,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20),
                )),
    );
  }
}
