// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AuthController on _AuthController, Store {
  late final _$isProgressLoginAtom =
      Atom(name: '_AuthController.isProgressLogin', context: context);

  @override
  bool get isProgressLogin {
    _$isProgressLoginAtom.reportRead();
    return super.isProgressLogin;
  }

  @override
  set isProgressLogin(bool value) {
    _$isProgressLoginAtom.reportWrite(value, super.isProgressLogin, () {
      super.isProgressLogin = value;
    });
  }

  late final _$isProgressRegisterAtom =
      Atom(name: '_AuthController.isProgressRegister', context: context);

  @override
  bool get isProgressRegister {
    _$isProgressRegisterAtom.reportRead();
    return super.isProgressRegister;
  }

  @override
  set isProgressRegister(bool value) {
    _$isProgressRegisterAtom.reportWrite(value, super.isProgressRegister, () {
      super.isProgressRegister = value;
    });
  }

  late final _$loginAsyncAction =
      AsyncAction('_AuthController.login', context: context);

  @override
  Future<void> login(String email, String password) {
    return _$loginAsyncAction.run(() => super.login(email, password));
  }

  late final _$loginViaProviderAsyncAction =
      AsyncAction('_AuthController.loginViaProvider', context: context);

  @override
  Future<void> loginViaProvider() {
    return _$loginViaProviderAsyncAction.run(() => super.loginViaProvider());
  }

  late final _$registerAsyncAction =
      AsyncAction('_AuthController.register', context: context);

  @override
  Future<void> register(String email, String password) {
    return _$registerAsyncAction.run(() => super.register(email, password));
  }

  @override
  String toString() {
    return '''
isProgressLogin: ${isProgressLogin},
isProgressRegister: ${isProgressRegister}
    ''';
  }
}
