import 'package:cloud_web/main.dart';
import 'package:cloud_web/main/data/password_controller.dart';
import 'package:cloud_web/models/user_credential.dart';
import 'package:cloud_web/static/app.dart';
import 'package:cloud_web/static/app_route.dart';
import 'package:cloud_web/static/app_string.dart';

import 'package:mobx/mobx.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

part 'auth_controller.g.dart';

class AuthController extends _AuthController with _$AuthController {}

abstract class _AuthController with Store {
  @observable
  bool isProgressLogin = false;

  @observable
  bool isProgressRegister = false;

  @action
  Future<void> login(String email, String password) async {
    isProgressLogin = true;
    try {
      var result = await App.supabase.client.auth
          .signIn(email: email, password: password);
      if (result.error != null) {
        App.showToast(result.error!.message);
        isProgressLogin = false;
      }
      getIt<PasswordController>()
          .loginUserCredential(credential: UserCredential(email: email))
          .then((value) {
        navigatorKey.currentState!.pushReplacementNamed(AppRoute.main);
        isProgressLogin = false;
      });
    } catch (e) {
      App.showToast(e.toString());
      isProgressLogin = false;
    }
  }

  @action
  Future<void> loginViaProvider() async {
    isProgressLogin = true;
    try {
      var result = await App.supabase.client.auth.signInWithProvider(
          Provider.google,
          options: AuthOptions(redirectTo: AppRoute.login));
    } catch (e) {
      App.showToast(e.toString());
      isProgressLogin = false;
    }
  }

  @action
  Future<void> register(String email, String password) async {
    isProgressRegister = true;
    try {
      var result = await App.supabase.client.auth.signUp(email, password);
      if (result.error != null) {
        App.showToast(result.error!.message);
        isProgressRegister = false;
      }
      getIt<PasswordController>()
          .registerUserCredential(credential: UserCredential(email: email))
          .then((value) {
        navigatorKey.currentState!.pushReplacementNamed(AppRoute.main);
        isProgressRegister = false;
      });
    } catch (e) {
      App.showToast(e.toString());
      isProgressRegister = false;
    }
  }
}
