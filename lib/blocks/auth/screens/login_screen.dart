import 'package:cloud_web/blocks/auth/components/auth_button.dart';
import 'package:cloud_web/blocks/auth/data/auth_controller.dart';
import 'package:cloud_web/blocks/auth/screens/register_screen.dart';
import 'package:cloud_web/static/app.dart';
import 'package:cloud_web/static/app_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../models/margin.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();

  void validateForm(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      getIt<AuthController>().login(email.text, password.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: SingleChildScrollView(
          child: Center(
            child: Form(
              key: _formKey,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.5,
                padding: const EdgeInsets.all(24),
                constraints: const BoxConstraints(maxWidth: 600),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.all(12),
                      width: MediaQuery.of(context).size.width * 0.4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextFormField(
                            decoration: const InputDecoration(
                                hintText: 'Enter email...'),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Enter some text';
                              }
                              return null;
                            },
                            controller: email,
                          ),
                          TextFormField(
                            decoration: const InputDecoration(
                                hintText: 'Enter password...'),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Enter some text';
                              }
                              return null;
                            },
                            controller: password,
                          ),
                        ],
                      ),
                    ),
                    Observer(builder: (context) {
                      return AuthButton(
                          text: AppString.login,
                          onPressed: () => validateForm(context),
                          isPressed: getIt<AuthController>().isProgressLogin);
                    }),
                    Margin(
                      geometry: const EdgeInsets.only(bottom: 12),
                      child: TextButton(
                          onPressed: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RegisterScreen())),
                          child: const Text(
                            AppString.register,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          )),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
