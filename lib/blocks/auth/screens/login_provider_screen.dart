import 'dart:math';

import 'package:animate_do/animate_do.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cloud_web/blocks/auth/components/auth_button.dart';
import 'package:cloud_web/static/app.dart';
import 'package:cloud_web/static/app_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/auth_controller.dart';

class LoginProviderScreen extends StatelessWidget {
  const LoginProviderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Row(
        children: [
          Expanded(
              child: Pulse(
            duration: const Duration(milliseconds: 3000),
            delay: const Duration(seconds: 0),
            infinite: true,
            child: const Center(
              child: FlutterLogo(
                size: 150,
              ),
            ),
          )),
          Expanded(
            child: Center(
              child: Container(
                margin: const EdgeInsets.only(right: 100),
                width: 500,
                height: 400,
                padding: const EdgeInsets.all(24),
                constraints: const BoxConstraints(maxWidth: 600),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AnimatedTextKit(
                        displayFullTextOnTap: true,
                        totalRepeatCount: 1,
                        animatedTexts: [
                          TyperAnimatedText('Sign in via GOOGLE',
                              speed: const Duration(milliseconds: 100),
                              textStyle: const TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 24)),
                        ]),
                    Center(
                      child: Observer(builder: (context) {
                        return AuthButton(
                            text: AppString.login,
                            onPressed: () =>
                                getIt<AuthController>().loginViaProvider(),
                            // onPressed: () =>
                            //     getIt<AuthController>().loginViaProvider(),
                            isPressed: getIt<AuthController>().isProgressLogin);
                      }),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
