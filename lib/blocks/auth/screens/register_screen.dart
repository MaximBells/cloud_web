import 'package:cloud_web/blocks/auth/components/auth_button.dart';
import 'package:cloud_web/blocks/auth/data/auth_controller.dart';

import 'package:cloud_web/static/app.dart';
import 'package:cloud_web/static/app_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class RegisterScreen extends StatelessWidget {
  RegisterScreen({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  final TextEditingController email = TextEditingController();
  final TextEditingController password1 = TextEditingController();
  final TextEditingController password2 = TextEditingController();

  void validateForm(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      getIt<AuthController>().register(email.text, password1.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pop(context),
        child: const Icon(Icons.arrow_back),
      ),
      backgroundColor: Colors.blue,
      body: Center(
        child: SingleChildScrollView(
          child: Center(
            child: Form(
              key: _formKey,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.5,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.all(12),
                      width: MediaQuery.of(context).size.width * 0.4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextFormField(
                            decoration: const InputDecoration(
                                hintText: 'Enter email...'),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Enter some text';
                              }
                              return null;
                            },
                            controller: email,
                          ),
                          TextFormField(
                            decoration: const InputDecoration(
                                hintText: 'Enter password...'),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Enter some text';
                              }
                              if (password2.text.isNotEmpty &&
                                  value != password2.text) {
                                return 'Passwords must be the same';
                              }
                              return null;
                            },
                            controller: password1,
                          ),
                          TextFormField(
                            decoration: const InputDecoration(
                                hintText: 'Enter password again...'),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Enter some text';
                              }
                              if (password1.text.isNotEmpty &&
                                  value != password1.text) {
                                return 'Passwords must be the same';
                              }
                              return null;
                            },
                            controller: password2,
                          ),
                        ],
                      ),
                    ),
                    Observer(builder: (context) {
                      return AuthButton(
                          text: AppString.register,
                          onPressed: () => validateForm(context),
                          isPressed:
                              getIt<AuthController>().isProgressRegister);
                    })
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
