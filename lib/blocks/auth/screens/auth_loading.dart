import 'package:animate_do/animate_do.dart';
import 'package:cloud_web/blocks/auth/data/auth_controller.dart';
import 'package:cloud_web/main/data/password_controller.dart';
import 'package:cloud_web/models/user_credential.dart';
import 'package:cloud_web/static/app.dart';
import 'package:cloud_web/static/app_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

class AuthLoading extends StatelessWidget {
  const AuthLoading({Key? key, required this.email}) : super(key: key);
  final String email;

  @override
  Widget build(BuildContext context) {
    return ReactionBuilder(
        child: Scaffold(
          body: Center(
            child: Spin(
                duration: const Duration(milliseconds: 1500),
                delay: const Duration(seconds: 1),
                infinite: true,
                child: FlutterLogo(
                  size: MediaQuery.of(context).size.width * 0.35,
                )),
          ),
        ),
        builder: (context) => reaction((_) {
              if (email.isEmpty) {
                Navigator.pushReplacementNamed(context, AppRoute.loading);
              } else {
                App.initApp().then((value) {
                  getIt<PasswordController>()
                      .isUserRegistered(email: email)
                      .then((registered) {
                    if (registered) {
                      getIt<PasswordController>()
                          .loginUserCredential(
                              credential: UserCredential(email: email))
                          .then((value) => Navigator.pushReplacementNamed(
                              context, AppRoute.main));
                    } else {
                      getIt<PasswordController>()
                          .registerUserCredential(
                              credential: UserCredential(email: email))
                          .then((value) => Navigator.pushReplacementNamed(
                              context, AppRoute.main));
                    }
                  });
                });
              }
            }, (result) {}));
  }
}
