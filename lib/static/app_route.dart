class AppRoute {
  static const loading = '/';
  static const login = '/login';
  static const register = '/register';
  static const successfull_login = '/successfull_login';
  static const successfull_register = '/successfull_register';
  static const main = '/main';
  static const accessScreen = '/access_screen';
}
