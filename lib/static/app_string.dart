class AppString {
  static const login = 'LOGIN';
  static const register = 'REGISTER';
  static const auth = 'AUTH';
  static const url = 'https://www.google.ru/';
  static const successfulRegister =
      'You have successfully registered a PIN code';
  static const wrongPassword = 'The entered password does not fit';
  static const array = 'array';
  static const site = 'Site';
  static const password = 'Password';
  static const createPinCode = 'Create Pin-Code';
  static const toCreateCredential =
      'To add a new entry, you first need to create a PIN code';
  static const enterPinCode = 'Enter your Pin-Code';
  static const toEnterCredential =
      'To log in to the app, you need to enter a PIN code';
  static const credentials = 'credentials';
}
