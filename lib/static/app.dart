import 'dart:convert';

import 'package:cloud_web/blocks/auth/data/auth_controller.dart';
import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart';
import 'package:flutter/material.dart' as material;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../main/data/password_controller.dart';

final getIt = GetIt.instance;

class App {
  static Future<void> initApp() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await Supabase.initialize(
        url: 'https://qrpshifqembmkrtkhtpi.supabase.co',
        anonKey:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InFycHNoaWZxZW1ibWtydGtodHBpIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NTQ4NDc0NTIsImV4cCI6MTk3MDQyMzQ1Mn0.bAieFnsGuhxDh3ALze5WJz7ita6F3ZqGhIs5ytFSULA');
    if (!getIt.isRegistered<SharedPreferences>()) {
      getIt.registerLazySingleton<SharedPreferences>(() => prefs);
    }
    if (!getIt.isRegistered<AuthController>()) {
      getIt.registerLazySingleton<AuthController>(() => AuthController());
    }

    if (!getIt.isRegistered<PasswordController>()) {
      getIt.registerLazySingleton<PasswordController>(
          () => PasswordController());
    }
    if (!getIt.isRegistered<Supabase>()) {
      getIt.registerLazySingleton<Supabase>(() => Supabase.instance);
    }
  }

  static Supabase get supabase => getIt<Supabase>();

  static String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  static String encrypt(String text, String password) {
    final key = Key.fromUtf8(generateMd5(password));
    final iv = IV.fromLength(16);
    final encrypter = Encrypter(AES(key));
    final encrypted = encrypter.encrypt(text, iv: iv);
    return encrypted.base64;
  }

  static String decrypt(String text, String password) {
    final key = Key.fromUtf8(generateMd5(password));
    final iv = IV.fromLength(16);
    final encrypter = Encrypter(AES(key));
    final encrypted = Encrypted.fromBase64(text);
    final content = encrypter.decrypt(encrypted, iv: iv);
    return content;
  }

  static void showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: material.Colors.white,
        textColor: material.Colors.black,
        webBgColor: '#ffffff',
        fontSize: 16.0);
  }
}
