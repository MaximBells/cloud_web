import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:flutter_slidable/flutter_slidable.dart';

import '../../../static/app.dart';
import '../../models/credential.dart';
import '../data/credential_controller.dart';
import '../data/password_controller.dart';

class CredentialRow extends StatelessWidget {
  CredentialRow({Key? key, required this.credential}) : super(key: key);

  final CredentialController controller = CredentialController();

  final Credential credential;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      startActionPane: ActionPane(
        dragDismissible: false,
        motion: const BehindMotion(),
        children: [
          SlidableAction(
            onPressed: (BuildContext context) {
              controller.getDelete();
              Future.delayed(const Duration(milliseconds: 250)).then((value) {
                getIt<PasswordController>()
                    .removeCredential(credential: credential);
                getIt<PasswordController>().updateCredentialList();
              });
            },
            backgroundColor: Colors.red,
            foregroundColor: Colors.white,
            icon: Icons.delete,
            label: 'Delete',
          ),
        ],
      ),
      endActionPane: ActionPane(
        dragDismissible: false,
        motion: const BehindMotion(),
        children: [
          SlidableAction(
            onPressed: (BuildContext context) {
              controller.getDelete();
              Future.delayed(const Duration(milliseconds: 250)).then((value) {
                getIt<PasswordController>()
                    .removeCredential(credential: credential);
                getIt<PasswordController>().updateCredentialList();
              });
            },
            backgroundColor: Colors.red,
            foregroundColor: Colors.white,
            icon: Icons.delete,
            label: 'Delete',
          ),
        ],
      ),
      child: Observer(builder: (context) {
        return AnimatedContainer(
          margin: controller.margin,
          duration: controller.duration,
          height: controller.height,
          child: controller.delete
              ? Container()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Row(
                        children: [
                          Text(credential.url),
                        ],
                      ),
                    ),
                    Expanded(
                        flex: 1,
                        child: GestureDetector(
                            onTap: () {
                              getIt<PasswordController>().copyToClipBoard(
                                  text: credential.login, context: context);
                            },
                            child: Text(
                              '*' * credential.login.length,
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 24),
                            ))),
                    Expanded(
                        flex: 1,
                        child: GestureDetector(
                            onTap: () {
                              getIt<PasswordController>().copyToClipBoard(
                                  text: credential.password, context: context);
                            },
                            child: Text(
                              '*' * credential.password.length,
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 24),
                            ))),
                    const Divider()
                  ],
                ),
        );
      }),
    );
  }
}
