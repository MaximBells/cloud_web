import 'package:cloud_web/blocks/auth/screens/login_screen.dart';
import 'package:cloud_web/static/app_route.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../static/app.dart';
import '../data/password_controller.dart';
import '../widgets/credential_row.dart';
import '../widgets/empty_row.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  /*
ConvexButton.fab(
        size: 128,
        iconSize: 36,
        sigma: 12,
        border: 4,
        thickness: 24,
        color: Colors.blue,
        icon: Icons.add,
        onTap: () => getIt<PasswordController>().pressAddCredential(context),
      )
   */
  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      return Scaffold(
        appBar: AppBar(
          title: Text(
            getIt<PasswordController>().email,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 12),
              child: IconButton(
                onPressed: () {
                  getIt<PasswordController>().clearCredentialList();
                  Navigator.pushReplacementNamed(context, AppRoute.login);
                },
                icon: const Icon(
                  Icons.home,
                  size: 42,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(
            Icons.add,
            size: 24,
          ),
          onPressed: () {
            getIt<PasswordController>().pressAddCredential(context);
          },
        ),
        body: Container(
          margin: const EdgeInsets.only(top: 20),
          child: Observer(
            builder: (context) {
              if (getIt<PasswordController>().credentialList.isEmpty) {
                return const EmptyRow();
              } else {
                return ListView.builder(
                    itemCount:
                        getIt<PasswordController>().credentialList.length,
                    itemBuilder: (context, index) {
                      return index == 0
                          ? Column(
                              children: [
                                const EmptyRow(),
                                CredentialRow(
                                    credential: getIt<PasswordController>()
                                        .credentialList[index]),
                                // const Divider(),
                              ],
                            )
                          : Column(
                              children: [
                                CredentialRow(
                                    credential: getIt<PasswordController>()
                                        .credentialList[index]),
                                // const Divider()
                              ],
                            );
                    });
              }
            },
          ),
        ),
      );
    });
  }
}
