import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobx/mobx.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import 'package:url_launcher/url_launcher_string.dart';
import 'favicon.dart' as favicon;

import '../../models/credential.dart';
import '../../models/user_credential.dart';
import '../../static/app.dart';
import '../../static/app_string.dart';
import '../screens/create_credential.dart';

part 'password_controller.g.dart';

enum AuthType { create, enter }

class PasswordController extends _PasswordController with _$PasswordController {
}

abstract class _PasswordController with Store {
  @observable
  bool isLoading = false;

  @observable
  String? pinCode;

  @observable
  AuthType authType = AuthType.enter;

  @observable
  ObservableList<Credential> credentialList = ObservableList.of([]);

  @observable
  UserCredential? userCredential;

  List<dynamic> savedList = [];

  @computed
  String get email {
    if (userCredential != null) {
      return userCredential!.email;
    } else {
      return "";
    }
  }

  @computed
  String get authTitle {
    switch (authType) {
      case AuthType.create:
        return AppString.createPinCode;

      case AuthType.enter:
        return AppString.enterPinCode;
    }
  }

  @computed
  String get authBody {
    switch (authType) {
      case AuthType.create:
        return AppString.toCreateCredential;

      case AuthType.enter:
        return AppString.toEnterCredential;
    }
  }

  @action
  Future<void> initCredentialList() async {
    //change here
    var result = await App.supabase.client
        .from(AppString.credentials)
        .select()
        .eq('email', email)
        .execute();
    if (result.error != null) {
      throw Exception(result.error!.message);
    }
    var dictionary = result.toJson();
    savedList = dictionary['data'][0]['data']['array'];
    pinCode = dictionary['data'][0]['pincode'];
    for (var element in savedList) {
      credentialList.add(Credential.fromString(App.decrypt(element, pinCode!)));
    }
    sortCredentialList();
  }

  @action
  Future<void> loginUserCredential({required UserCredential credential}) async {
    userCredential = credential;
    await initCredentialList();
  }

  @action
  Future<void> registerUserCredential(
      {required UserCredential credential}) async {
    userCredential = credential;
    pinCode = App.generateMd5(Random.secure().nextDouble().toString());
    var result = await App.supabase.client.from(AppString.credentials).insert([
      {
        'email': email,
        'data': {'array': []},
        'pincode': pinCode
      }
    ]).execute();
    if (result.error != null) {
      throw Exception(result.error!.message);
    }
  }

  @action
  Future<bool> isUserRegistered({required String email}) async {
    var result = await App.supabase.client
        .from(AppString.credentials)
        .select()
        .eq('email', email)
        .execute(count: CountOption.exact);
    if (result.error != null || result.count == null) {
      throw Exception(result.error!.message);
    }
    if (result.count == 0){
      return false;
    } else {
      return true;
    }
  }

  @action
  void clearCredentialList() {
    credentialList.clear();
  }

  @action
  Future<void> addCredential({required Credential credential}) async {
    credentialList.add(credential);
    sortCredentialList();
    await saveCredentialList(credential: credential);
  }

  @action
  void removeCredential({required Credential credential}) {
    credentialList.remove(credential);
    sortCredentialList();
    saveCredentialList(credential: credential);
  }

  @action
  void updateCredentialList() => credentialList = credentialList;

  @action
  Future<void> saveCredentialList({required Credential credential}) async {
    //change here
    savedList.clear();
    for (var element in credentialList) {
      savedList.add(App.encrypt(element.toString(), pinCode!));
    }
    var result = await App.supabase.client.from(AppString.credentials).update({
      'data': {'array': savedList}
    }).match({'email': email}).execute();
    if (result.error != null) {
      throw Exception(result.error!.message);
    }
  }

  @action
  void sortCredentialList() =>
      credentialList.sort((a, b) => b.date.compareTo(a.date));

  @action
  void pressAddCredential(BuildContext buildContext) {
    Navigator.push(buildContext,
        CupertinoPageRoute(builder: (context) => CreateCredential()));
  }

  @action
  String? checkSite(String? value) {
    if (value == null || value.isEmpty) {
      return 'Cant be empty';
    }
    if (Uri.tryParse(value) == null) {
      return 'Must be a real URL';
    }
    return null;
  }

  @action
  String? checkLogin(String? value) {
    if (value == null || value.isEmpty) {
      return 'Cant be empty';
    }
    if (value.length < 2) {
      return 'Length must be more than 2';
    }
    return null;
  }

  @action
  String? checkPassword(String? value) {
    if (value == null || value.isEmpty) {
      return 'Cant be empty';
    }
    if (value.length < 2) {
      return 'Length must be more than 2';
    }
    return null;
  }

  @action
  Future<void> createCredential(
      {required String url,
      required String login,
      required String password,
      required BuildContext context}) async {
    if (!(await canLaunchUrlString(url))) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        action: SnackBarAction(
          label: 'OKAY',
          textColor: Colors.white,
          onPressed: () {
            // Some code to undo the change.
          },
        ),
        content: const Text(
          'Must be a real url',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
        ),
        backgroundColor: Colors.red,
      ));
    } else {
      try {
        isLoading = true;
        await addCredential(
            credential: Credential(
                url: url,
                login: App.encrypt(login, pinCode!),
                password: App.encrypt(password, pinCode!),
                date: DateTime.now()));
        isLoading = false;
        Navigator.pop(context);
      } catch (e) {
        print(e);
        isLoading = false;
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          action: SnackBarAction(
            label: 'OKAY',
            textColor: Colors.white,
            onPressed: () {
              // Some code to undo the change.
            },
          ),
          content: Text(
            e.toString(),
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          backgroundColor: Colors.red,
        ));
      }
    }
  }

  Future<String?> loadIcon(String url) async {
    var iconData = await favicon.Favicon.getBest(url);
    if (iconData != null) {
      return iconData.url;
    } else {
      return null;
    }
  }

  @action
  void setAuthType(AuthType type) => authType = type;

  @action
  void copyToClipBoard({required String text, required BuildContext context}) {
    Clipboard.setData(ClipboardData(text: App.decrypt(text, pinCode!)))
        .then((value) => ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: const Duration(milliseconds: 1500),
            backgroundColor: Colors.green,
            action: SnackBarAction(
              label: 'Okay',
              onPressed: () {},
            ),
            content: const Text(
              'Copied!',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
            ))));
  }
}
