import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';

part 'credential_controller.g.dart';

class CredentialController extends _CredentialController
    with _$CredentialController {}

abstract class _CredentialController with Store {
  @observable
  bool delete = false;

  @computed
  EdgeInsetsGeometry get margin {
    if (delete == true) {
      return const EdgeInsets.all(0);
    } else {
      return const EdgeInsets.all(12);
    }
  }

  @computed
  Duration get duration {
    if (delete == true) {
      return const Duration(milliseconds: 250);
    } else {
      return const Duration(milliseconds: 0);
    }
  }

  @computed
  double get height {
    if (delete == true){
      return 0;
    } else {
      return 100;
    }
  }

  @action
  void getDelete() => delete = true;
}
