// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credential_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CredentialController on _CredentialController, Store {
  Computed<EdgeInsetsGeometry>? _$marginComputed;

  @override
  EdgeInsetsGeometry get margin =>
      (_$marginComputed ??= Computed<EdgeInsetsGeometry>(() => super.margin,
              name: '_CredentialController.margin'))
          .value;
  Computed<Duration>? _$durationComputed;

  @override
  Duration get duration =>
      (_$durationComputed ??= Computed<Duration>(() => super.duration,
              name: '_CredentialController.duration'))
          .value;
  Computed<double>? _$heightComputed;

  @override
  double get height =>
      (_$heightComputed ??= Computed<double>(() => super.height,
              name: '_CredentialController.height'))
          .value;

  late final _$deleteAtom =
      Atom(name: '_CredentialController.delete', context: context);

  @override
  bool get delete {
    _$deleteAtom.reportRead();
    return super.delete;
  }

  @override
  set delete(bool value) {
    _$deleteAtom.reportWrite(value, super.delete, () {
      super.delete = value;
    });
  }

  late final _$_CredentialControllerActionController =
      ActionController(name: '_CredentialController', context: context);

  @override
  void getDelete() {
    final _$actionInfo = _$_CredentialControllerActionController.startAction(
        name: '_CredentialController.getDelete');
    try {
      return super.getDelete();
    } finally {
      _$_CredentialControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
delete: ${delete},
margin: ${margin},
duration: ${duration},
height: ${height}
    ''';
  }
}
